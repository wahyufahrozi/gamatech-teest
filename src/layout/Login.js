import React, {useState, useEffect} from "react";
import Axios from 'axios'
import './assets/Style.css'

const Login = () => {
  const [login, setLogin] = useState({
    username : "",
    password : ""
  })

  const [data, setData] = useState([])

  const handleChange = e =>{
    setLogin({ ...login, [e.target.name]: e.target.value });
  }

  useEffect( ()=>{
    const url = `https://private-03e1f5-wahyufahrozi.apiary-mock.com/login`
    Axios.get(url)
    .then( response => {
      setData(response.data)
    })
    .catch(err => console.log(err))

  },[])

  const handleSubmit = () =>{
    data.map( (r,i) =>{
      setTimeout(()=>{
        if (login.username === r.username && login.password === r.password){
          console.log(`login berhasil`)
        } else{
          console.log(`gagal`)
        }
      }, 5000)
    })

    const url = `https://private-03e1f5-wahyufahrozi.apiary-mock.com/login`
    Axios.get(url)
    .then( response => {
      setData(response.data)
    })
    .catch(err => console.log(err))
  }

  console.log(`ini`, data)

  return (
    <div className="box" style={{}}>
        <h1>Login </h1>
        <input className="user" type="text" name="username" value={login.username} placeholder="Username" onChange={handleChange}/>
        <input className="pass" type="password" name="password" value={login.password} placeholder="Password" onChange={handleChange}/>
        <button onClick={handleSubmit} className="submit" > Login </button>
    </div>
  );
};

export default Login;
