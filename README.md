

<p align="center">
  <a href="https://nodejs.org/">
    <img title="Restful API" height='200' src="https://cdn4.iconfinder.com/data/icons/logos-3/600/React.js_logo-512.png">
  </a>
</p>
<p align="center">
    <a href="#">
    <img alt="David" src="https://img.shields.io/david/dev/wahyufahrozi/FrontEndReact">
  </a>
  
  <a href="#">
    <img title="Open Source Love" src="https://badges.frapsoft.com/os/v1/open-source.svg?v=102">
  </a>
  <a href="#">
    <img title="Repo Size" src="https://img.shields.io/github/repo-size/wahyufahrozi/FrontEndReact">
  </a>
  <a href="https://github.com/prettier/prettier"><img src="https://img.shields.io/badge/styled_with-prettier-ff69b4.svg"></a>
</p>

## Table Of Contents

- [Table Of Contents](#table-of-contents)
- [Build Setup](#build-setup)
- [Stacks](#stacks)
- [Dependencies](#dependencies)
- [Aplication Structure](#aplication-structure)

## Build Setup

1. Clone repository
   `$ git clone https://gitlab.com/wahyufahrozi/gamatech-test`

2. Install depedencies

```bash
# with npm
$ npm install

# or with yarn
$ yarn install
```

3. Start Web server

```bash
$ npm start
```

## Stacks

- ReactJS

## Dependencies


- [react-router-dom](https://www.npmjs.com/package/react-router-dom) - DOM bindings for React Router.


## Aplication Structure

- `index.js` - Entry point of our aplication
- `src/layout` - This folder containt files that all page
- `src/Assets` - This folder contain assets for this web, such as style

---




Copyright © 2019 by Wahyu Fahrozi Rezeki Ramadhan
